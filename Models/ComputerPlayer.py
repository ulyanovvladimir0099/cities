from random import choice
from itertools import chain
from Models.Player import Player


class ComputerPlayer(Player):
    """Класс игрок - Компьютер"""

    def __init__(self, name: str):
        super(ComputerPlayer, self).__init__(name)

    def make_move(self, city_names_collection: dict, player_moves_collection: dict,
                  city_name_first_letter: str = None) -> str or None:
        """Выбор хода компьютером (выбор города из списка)"""

        available_city_names_list = list()
        if city_name_first_letter is None:
            available_city_names_list = list(chain(*city_names_collection.values()))
        elif city_name_first_letter in city_names_collection:
            named_cities_list = list(city_name for city_name in chain(*player_moves_collection.values())
                                     if city_name.startswith(city_name_first_letter))
            available_city_names_list = list(city_name for city_name in city_names_collection[city_name_first_letter]
                                             if city_name not in named_cities_list)

        if not available_city_names_list:
            return None

        named_city = choice(available_city_names_list)
        player_moves_collection[super(ComputerPlayer, self).name].append(named_city)

        return named_city
