from abc import ABC, abstractmethod


class Player(ABC):
    def __init__(self, name: str):
        self.__name = name

    @property
    def name(self) -> str:
        return self.__name

    @abstractmethod
    def make_move(self, city_names_collection: dict, player_moves_collection: dict,
                  city_name_first_letter: str = None) -> str or None:
        pass
