from itertools import chain
from Utilities.StrUtilities import StrUtilities
from Models.Player import Player


class UserPlayer(Player):
    """Класс игрок-пользователь"""
    def __init__(self, name: str):
        super(UserPlayer, self).__init__(name)

    def make_move(self, city_names_collection: dict, player_moves_collection: dict,
                  city_name_first_letter: str = None) -> str or None:
        available_city_names_list = list()
        named_cities_list = list()
        if city_name_first_letter is None:
            available_city_names_list = list(chain(*city_names_collection.values()))
        elif city_name_first_letter in city_names_collection:
            available_city_names_list = city_names_collection[city_name_first_letter]
            named_cities_list = list(city_name for city_name in chain(*player_moves_collection.values())
                                     if city_name.startswith(city_name_first_letter))

        for i in range(1, 6):
            print("[Попытка #", i, "] Введите город на букву ", city_name_first_letter, ": ", sep="", end="")
            named_city = StrUtilities.capitalize(input())
            if named_city not in available_city_names_list:
                print("Такого города в России нет!")
                named_city = str()
            elif named_city in named_cities_list:
                print("Город уже был назван ранее!")
                named_city = str()
            else:
                break

        if not named_city:
            return None

        player_moves_collection[super(UserPlayer, self).name].append(named_city)

        return named_city
