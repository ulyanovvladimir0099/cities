from datetime import datetime


class Game:
    """Класс игра основная логика игры и методы выбора имени города"""
    def __init__(self, players: list, city_names_collection: dict):
        self.__players = players
        self.__city_names_collection = city_names_collection
        self.__player_moves_collection = {player.name: list() for player in self.__players}
        self.__history = dict.fromkeys(["startTime", "endTime", "playerMoves", "winners"])

    @property
    def history(self):
        return self.__history

    def play(self):
        """Оперделяем стартовое время игры и условия ходов а также недопустимые символы для имен городов"""

        start_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        is_first_move = True
        invalid_first_letters = ["Ы", "Ь", "Ъ"]

        self.__history["startTime"] = start_time

        print("[", start_time, "] Игра в \"Города\" начата", sep="")
        while True:
            print("Ход делает игрок \"", self.__players[0].name, "\":", sep="")
            if is_first_move:
                last_named_city = self.__players[0].make_move(self.__city_names_collection,
                                                              self.__player_moves_collection)
                is_first_move = False
            else:
                last_named_city = self.__players[0].make_move(self.__city_names_collection,
                                                              self.__player_moves_collection, next_city_first_letter)

            if last_named_city is None:
                print("Игрок \"", self.__players[0].name, "\" не смог сделать очередной ход и проиграл!", sep="")
                break
            else:
                if last_named_city == "Йошкар-Ола":
                    invalid_first_letters.append("Й")
                next_city_first_letter = last_named_city[-1].upper()
                if next_city_first_letter in invalid_first_letters:
                    next_city_first_letter = last_named_city[0]

            print("Игрок назвал город ", last_named_city, sep="")

            self.__players.append(self.__players.pop(0))

        end_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

        self.__history["endTime"] = end_time
        self.__history["playerMoves"] = self.__player_moves_collection
        self.__history["winners"] = list(player.name for player in self.__players
                                         if player.name != self.__players[0].name)

        print("[", end_time, "] Игра в \"Города\" окончена", sep="")
