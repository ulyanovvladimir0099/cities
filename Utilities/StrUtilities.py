class StrUtilities:
    """Класс форматирования данных - замена регистров"""

    @staticmethod
    def capitalize(source_string: str) -> str:
        resulting_string = str().join([source_string_token.capitalize() for source_string_token in
                                       source_string.strip().split(" ")]).replace("-На-", "-на-")

        return resulting_string
