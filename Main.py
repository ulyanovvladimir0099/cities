from Models.UserPlayer import UserPlayer
from Models.ComputerPlayer import ComputerPlayer
from Services.GameManager import GameManager

if __name__ == '__main__':
    cities_game_manager = GameManager([ComputerPlayer('Компьютер'), UserPlayer('Пользователь')])
    cities_game_manager.load_data()
    cities_game_manager.start_game()
    cities_game_manager.save_data()
