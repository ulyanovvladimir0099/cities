from Models.Game import Game
from Services.GameDataSaver import GameDataSaver
from Services.GameDataLoader import GameDataLoader


class GameManager:
    """ Класс извлечения списка городов из интернета и запуска игры"""

    def __init__(self, players: list):
        self.__players = players
        self.__city_names_collection = dict()
        self.__game = Game(self.__players, self.__city_names_collection)

    def load_data(self):
        cities_file_path = "Files/RussianCities.json"
        cities_file_url = "https://raw.githubusercontent.com/pensnarik/russian-cities/master/russian-cities.json"

        self.__city_names_collection = GameDataLoader.load_city_names_collection_from_local_file(cities_file_path)

        if not self.__city_names_collection:
            self.__city_names_collection = GameDataLoader.load_city_names_collection_from_remote_file(cities_file_url)

    def save_data(self):
        cities_file_path = "Files/RussianCities.json"
        games_history_file_path = "Files/GamesHistory.json"

        GameDataSaver.save_city_names_collection_to_local_file(cities_file_path, self.__city_names_collection)
        GameDataSaver.save_game_history_to_local_file(games_history_file_path, self.__game.history)

    def start_game(self):
        self.__game = Game(self.__players, self.__city_names_collection)
        self.__game.play()
