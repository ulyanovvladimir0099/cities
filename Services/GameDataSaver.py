from codecs import open
from json import dump
from Services.GameDataLoader import GameDataLoader


class GameDataSaver:
    """ Класс сохранения всех ходов игроков """

    @staticmethod
    def save_city_names_collection_to_local_file(file_path: str, city_names_collection: dict):
        with open(file_path, "w", encoding="utf8") as file:
            dump(city_names_collection, file, ensure_ascii=False)

    @staticmethod
    def save_game_history_to_local_file(file_path: str, game_history: dict):
        games_history_list = GameDataLoader.load_games_history_list_from_local_file(file_path)

        games_history_list.append(game_history)

        with open(file_path, "w", encoding="utf8") as file:
            dump(games_history_list, file, ensure_ascii=False)

