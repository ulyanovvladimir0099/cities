from codecs import open
from json import load, loads
from Services.HTTPDataReceiver import HTTPDataReceiver


class GameDataLoader:
    """ Класс загрузчика данных о городах - запись в файл после получения ссылки """

    @staticmethod
    def load_city_names_collection_from_local_file(file_path: str) -> dict:
        with open(file_path, "r", encoding="utf8") as file:
            city_names_collection = load(file)

        return city_names_collection

    @staticmethod
    def load_city_names_collection_from_remote_file(file_url: str) -> dict:
        city_names_collection = dict()
        city_information_records_list = loads(HTTPDataReceiver.get_data(file_url))

        for current_city_information_record in city_information_records_list:
            current_city_name = current_city_information_record["name"]
            if current_city_name[0] not in city_names_collection:
                city_names_collection[current_city_name[0]] = list()
            else:
                city_names_collection[current_city_name[0]].append(current_city_name)

        return city_names_collection

    @staticmethod
    def load_games_history_list_from_local_file(file_path: str) -> list:
        with open(file_path, "r", encoding="utf8") as file:
            games_history_list = load(file)

        return games_history_list
