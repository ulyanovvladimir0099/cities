import requests


class HTTPDataReceiver:
    """ Класс доступа по ссылке """

    @staticmethod
    def get_data(url: str) -> str:
        return requests.get(url).text
